# pcd - Podcast Daemon

pcd is a Linux podfetching daemon.

## Build/installation

```sh
$ git clone https://gitlab.com/ajak/pcd.git
$ cd pcd
$ ./autogen.sh
$ make
```

### Dependencies

pcd depends on:

* curl
* libxml2
* libpthread

## Usage

```sh
~ $ pcd -h
usage: ./pcd [options]
  options:
    -h, --help                 Print this help
    -s, --start                Start the daemon
    -k, --kill                 Kill the daemon
    -f, --foreground           Don't run as daemon
    -a, --add [url]            Add a podcast to download
    -c, --config [path]        Specify path to config file
    -p, --pidfile [path]       Specify path to pidfile
    -U, --socket [path]        Specify path to Unix socket file
    -D, --podcast_dir [path]   Specify path to podcast storage directory
```

## Configuration

pcd looks for a configuration file at `/etc/pcd.conf`. If a config file was
specified on the command line, the config at `/etc/pcd.conf` will be ignored
and pcd will use the command line config. Any config options specified as
command line options override options specified in the used config file.

Configuration options, with default options:

```
user [user]        Specify the user to run as. Default root
pidfile [path]     Specify path to use for pidfile. Default '/var/run/pcd.pid'
socket [path]      Specify path to Unix socket file. Default '/var/run/pcd.socket'
podcast_dir [path] Specify podcast storage directory. Default '/var/lib/pcd'
proxy [proxy]      Specify proxy URL, must include protocol. No default.
proxyport [port]   Specify port to connect to proxy. No default.
```
