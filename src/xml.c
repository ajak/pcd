/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#define _XOPEN_SOURCE

#include <string.h>
#include <syslog.h>
#include <time.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "downloader.h"
#include "pcd_output.h"
#include "podcast.h"
#include "util.h"
#include "xml.h"

static xmlXPathObjectPtr create_xpath_obj(xmlDocPtr xml_doc, const char *xpath)
{
	xmlXPathContextPtr xpath_ctxt;
	xmlXPathObjectPtr xpath_obj;

	xpath_ctxt = xmlXPathNewContext(xml_doc);

	if (!xpath_ctxt)
		return NULL;

	xpath_obj = xmlXPathEvalExpression((xmlChar *) xpath, xpath_ctxt);

	xmlXPathFreeContext(xpath_ctxt);

	return xpath_obj;
}

static char *xml_get_title(xmlDocPtr xml_doc)
{
	xmlXPathObjectPtr xpath_obj;
	xmlNodePtr node;
	int node_len;
	char *title;

	if (!xml_doc)
		return NULL;

	xpath_obj = create_xpath_obj(xml_doc, "//rss//channel//title");

	if (!xpath_obj) {
		return NULL;
	} else if (xmlXPathNodeSetIsEmpty(xpath_obj->nodesetval)) {
		xmlXPathFreeObject(xpath_obj);
		return NULL;
	}

	node_len = xpath_obj->nodesetval->nodeNr;

	/* Need to find the title element just under the <channel> element. To
	 * do this, loop through the xpath results until it's found */
	for (int i=0; i < node_len; i++) {
		node = xpath_obj->nodesetval->nodeTab[i];

		if (strcmp((char *) node->parent->name, "channel") == 0) {
			// Found it
			title = strdup((char *) node->children->content);
			break;
		}
	}

	xmlXPathFreeObject(xpath_obj);

	return title;
}

/* The URL to the podcast mp3 is contained in a node called enclosure, loop
 * through the linked list to find the enclosure tag */
static xmlNodePtr xml_get_url_node(xmlNodePtr xml_ep)
{
	xmlNodePtr node = xml_ep->children;

	while (strcmp((char *) node->name, "enclosure") != 0) {
		node = node->next;

		if (!node)
			break;
	}

	return node;
}

static char *xml_get_ep_url(xmlNodePtr xml_ep)
{
	xmlNodePtr url_node;
	xmlAttrPtr enclosure_attrs;

	url_node = xml_get_url_node(xml_ep);

	if (url_node)
		enclosure_attrs = url_node->properties;
	else
		return NULL;

	while (strcmp((char *) enclosure_attrs->name, "url") != 0)
		enclosure_attrs = enclosure_attrs->next;

	return (char *) enclosure_attrs->children->content;
}

// Clean up the titles so they can be used as filenames
// TODO - more characters
static void sanitize_title(char *title)
{
	char *replace;

	while ((replace = strchr(title, '/')))
		*replace = '-';
}

// xml_node should be a node corresponding parent <item> for an episode
static char *find_ep_title(xmlNodePtr xml_node)
{
	xml_node = xml_node->children;

	while ((xml_node = xml_node->next)) {
		if (!xml_node->name)
			break;

		if (strcmp((char *) xml_node->name, "title") == 0)
			return (char *) xml_node->children->content;
	}

	return NULL;
}

static struct tm xml_get_episode_date(xmlNodePtr xml_ep)
{
	struct tm time = {0};
	char *timestr;

	xml_ep = xml_ep->children;

	// TODO - can this loop infinitely?
	while (strcmp((char *) xml_ep->name, "pubDate") != 0)
		xml_ep = xml_ep->next;
	// xml_ep now points at the pubDate node

	timestr = (char *) xml_ep->children->content;

	// Offset the string reading 5 characters, we don't care about
	// the leading 3 letter day, comma, or space
	strptime(timestr, "%a, %d %b %Y %T %Z", &time);

	return time;
}

static int create_episode(xmlNodePtr xml_ep, struct episode *e)
{
	char *title;
	char *url;
	char *ext;

	if ((title = find_ep_title(xml_ep))) {
		e->title = strdup(title);
		sanitize_title(e->title);
	} else {
		output(LOG_ERR, "Couldn't find episode title");
		return EXIT_FAILURE;
	}

	if ((url = xml_get_ep_url(xml_ep))) {
		e->url = strdup(url);
	} else {
		output(LOG_ERR, "Couldn't find URL for %s", e->title);
		return EXIT_FAILURE;
	}

	if ((ext = get_episode_ext(e))) {
		e->ext = ext;
	} else {
		output(LOG_ERR, "Couldn't get extension for %s/%s", e->p->title, e->title);
		return EXIT_FAILURE;
	}

	e->time = xml_get_episode_date(xml_ep);

	return EXIT_SUCCESS;
}

static int xml_to_episodes(char *podcast_dir, xmlDocPtr xml_doc, struct podcast *pc)
{
	xmlXPathObjectPtr xpath_obj;
	char *path = NULL;
	int ep_num;

	xpath_obj = create_xpath_obj(xml_doc, "//rss//channel//item");

	if (!xpath_obj) {
		return EXIT_FAILURE;
	} else if (xmlXPathNodeSetIsEmpty(xpath_obj->nodesetval)) {
		xmlXPathFreeObject(xpath_obj);
		return EXIT_FAILURE;
	}

	ep_num = xpath_obj->nodesetval->nodeNr;

	pc->episodes = malloc((unsigned long)(ep_num) * sizeof(struct episode));

	for (int i = 0; i < ep_num; i++) {
		pc->episodes[i].p = pc;

		if (create_episode(xpath_obj->nodesetval->nodeTab[i], &pc->episodes[i]) != EXIT_SUCCESS) {
			xmlXPathFreeObject(xpath_obj);
			return EXIT_FAILURE;
			/* Dangling podcast memory created here will be cleaned
			 * up after this podcast_add realizes this failed */
		}

		path = get_episode_path(podcast_dir, &pc->episodes[i]);

		if (!path) {
			output(LOG_ERR, "Couldn't get path for episode");
			return EXIT_FAILURE;
		}

		pc->episodes[i].bytes_recvd = filesize(path);

		pc->e_count++;

		free(path);
 	}

	xmlXPathFreeObject(xpath_obj);

	return EXIT_SUCCESS;
}

int xml_to_podcast(char *podcast_dir, xmlDocPtr xml_doc, struct podcast *pc)
{
	pc->title = xml_get_title(xml_doc);

	if (xml_to_episodes(podcast_dir, xml_doc, pc) != EXIT_SUCCESS)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
