/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef QUEUE_H
#define QUEUE_H

#include <pthread.h>

#include "pcd.h"

struct queue_item {
	void *data;
	struct queue_item *next;
};

struct queue {
	struct queue_item *queue_head;
	pthread_cond_t cond;
	pthread_mutex_t _mutex;

	/* In the case that queue_cleanup() is called on the queue when the
	 * queue is not empty, this function allows for more complex freeing of
	 * memory pointed to by item->data than a simple free() */
	void (*free)(void *data);
};

int queue_add(struct queue *q, void *data, size_t size);
void *q_get_item(struct queue *q);
struct queue *queue_init(void (*free)(void *data));
void queue_cleanup(struct queue *q);

#endif /* QUEUE_H */
