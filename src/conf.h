/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef CONF_H
#define CONF_H

#include <stdbool.h>

#include "pcd_string.h"

#define DEFAULT_CONFIG_PATH "/etc/pcd.conf"

// This should always be equal to the length of cfg_key enum - 1 (we don't want
// a value for PCD_CFG_CONFIG)
// TODO - better way of handling this
#define CONFIG_DICT_LENGTH 7

enum cfg_type {
	PCD_BOOL,
	PCD_STRING
};

// Possible config values
enum cfg_key {
	PCD_CFG_USER,
	PCD_CFG_SOCKET,
	PCD_CFG_PIDFILE,
	PCD_CFG_PODCAST_DIR,
	PCD_CFG_PROXY,
	PCD_CFG_STATEFILE,
	PCD_CFG_FOREGROUND,
	PCD_CFG_CONFIG
};

// Inspired by transmission's config handling
// https://github.com/transmission/transmission/blob/a762c770f28870728ed8c93e1fb048e631c8b4f1/libtransmission/variant.h#L75
struct pcd_cfg {
	enum cfg_type type;
	enum cfg_key key;

	union {
		string *str;
		bool b;

		struct {
			/* Basically unused pending CONFIG_DICT_LENGTH TODO */
			size_t len;
			struct pcd_cfg *dict;
		} cfg;
	} val;
};

void cfg_destroy(struct pcd_cfg *cfg);
void *cfg_get_val(struct pcd_cfg *cfg, enum cfg_key key);
char *cfg_get_val_buf(struct pcd_cfg *cfg, enum cfg_key key);
int update_cfg(struct pcd_cfg *cfg, enum cfg_type type, enum cfg_key key, void *new_val);
int update_cfg_from_str(struct pcd_cfg *cfg, string *str);
int update_cfg_from_file(struct pcd_cfg *cfg, string *cfg_path);
struct pcd_cfg *cfg_from_str(string *str);
struct pcd_cfg *cfg_from_file(char *cfg_path);

#endif /* CONF_H */
