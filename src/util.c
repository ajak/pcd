/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "util.h"

long filesize(char *path)
{
	FILE *f;
	long size;

	f = fopen(path, "r");

	if (!f)
		return -1;

	fseek(f, 0L, SEEK_END);
	size = ftell(f);

	fclose(f);

	return size;
}

bool file_exists(char *path)
{
	return access(path, F_OK) == 0;
}

void strip_newline(char *str)
{
	char *new_str = strstr(str, "\n");

	if (new_str)
		strncpy(new_str, "\0", 1);
}
