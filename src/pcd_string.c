/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <string.h>

#include "pcd_string.h"

string *new_string(char *buf)
{
	string *tmp = NULL;

	/* Return a null struct if the given string is null */
	if (!*buf)
		return tmp;

	tmp = malloc(sizeof(string));
	tmp->len = strlen(buf) + 1;
	tmp->buf = malloc(tmp->len);
	memcpy(tmp->buf, buf, tmp->len);

	return tmp;
}

char *str_get_buf(string *str)
{
	return str->buf;
}

void free_string(string *str)
{
	if (!str || !str->buf)
		return;

	/* Free the string if it exists */
	if (*str->buf)
		free(str->buf);

	free(str);

	str = NULL;
}
