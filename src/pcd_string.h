/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef PCD_STRING_H
#define PCD_STRING_H

#include <stddef.h>

/* TODO - A (probably small) memory optimization exists here - we could track
 * whether the char buffer passed to new_string() is stored on the stack or
 * needs to be strdup'd onto the heap. For example, strings in argv are passed
 * into the config struct, and there's no reason for these strings to be stored
 * on the heap.
 */
typedef struct {
	size_t len;
	char *buf;
} string;

string *new_string(char *buf);
char *str_get_buf(string *str);
void free_string(string *str);

#endif /* PCD_STRING_H */
