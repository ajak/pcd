/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <assert.h>
#include <stdbool.h>
#include <string.h>

#include "conf.h"
#include "pcd_string.h"

int main(void)
{
	char *buf1 = "user pcd\npidfile pcd.pid\n";
	char *buf2 = "pidfile pid\n";
	string *str1 = new_string(buf1);
	string *str2 = new_string(buf2);
	struct pcd_cfg *cfg = NULL;

	cfg_destroy(cfg);

	cfg = cfg_from_str(str1);

	assert(cfg);

	/* Test some values */
	assert(strcmp("pcd", cfg_get_val_buf(cfg, PCD_CFG_USER)) == 0);
	assert(strcmp("pcd.pid", cfg_get_val_buf(cfg, PCD_CFG_PIDFILE)) == 0);
	assert(cfg_get_val_buf(cfg, PCD_CFG_PROXY) == NULL);

	update_cfg_from_str(cfg, str2);
	assert(strcmp("pid", cfg_get_val_buf(cfg, PCD_CFG_PIDFILE)) == 0);

	update_cfg(cfg, PCD_STRING, PCD_CFG_PIDFILE, new_string("pcd.pid"));
	assert(strcmp("pcd.pid", cfg_get_val_buf(cfg, PCD_CFG_PIDFILE)) == 0);

	assert(cfg_get_val(cfg, PCD_CFG_FOREGROUND) == false);
	update_cfg(cfg, PCD_BOOL, PCD_CFG_FOREGROUND, (void *) true);
	assert(((bool) cfg_get_val(cfg, PCD_CFG_FOREGROUND)) == true);

	cfg_destroy(cfg);
	free_string(str2);
	free_string(str1);
}
