/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <curl/curl.h>

#include "command.h"
#include "conf.h"
#include "pcd.h"
#include "pcd_output.h"

static int url_ok(char *url)
{
	CURLU *url_h = curl_url();
	int ret;

	ret = curl_url_set(url_h, CURLUPART_URL, url, 0);

	curl_url_cleanup(url_h);

	/* CURLUE_OK may not equal 1 for true, so evaluating this will give us
	   the exit status of curl_url_set as a boolean */
	return ret == CURLUE_OK;
}

static void print_help(char *name)
{
	printf("usage: %s [options]\n", name);
	printf("  options:\n");
	printf("    -h, --help               Print this help\n");
	printf("    -s, --start              the daemon\n");
	printf("    -k, --kill               Kill the daemon\n");
	printf("    -f, --foreground         Don't run as daemon\n");
	printf("    -a, --add [url]          Add a podcast to download\n");
	printf("    -c, --config [path]      Specify path to config file\n");
	printf("    -p, --pidfile [path]     Specify path to pidfile\n");
	printf("    -U, --socket [path]      Specify path to Unix socket file\n");
	printf("    -D, --podcast_dir [path] Specify path to podcast storage directory\n");
	printf("    -l, --logmask [mask]     Specify log mask\n");
}

static int pcd_setlogmask(char *strmask)
{
	/* TODO - accept partial values, i.e "eme" or "al" */
	if (strcmp("emerg", strmask) == 0) {
		setlogmask(LOG_EMERG);
	} else if (strcmp("alert", strmask) == 0) {
		setlogmask(LOG_ALERT);
	} else if (strcmp("crit", strmask) == 0) {
		setlogmask(LOG_CRIT);
	} else if (strcmp("err", strmask) == 0) {
		setlogmask(LOG_ERR);
	} else if (strcmp("warning", strmask) == 0) {
		setlogmask(LOG_WARNING);
	} else if (strcmp("notice", strmask) == 0) {
		setlogmask(LOG_NOTICE);
	} else if (strcmp("info", strmask) == 0) {
		setlogmask(LOG_INFO);
	} else if (strcmp("debug", strmask) == 0) {
		setlogmask(LOG_DEBUG);
	} else {
		printf("Bad logmask setting\n");

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
	int ret = 0;
	int b = 0;
	bool start = 0;
	bool kill = 0;
	bool err = 0;
	int cmd = 0;
	struct pcd_cfg *cfg = NULL;
	string *user_cfg = NULL;
	string *url = NULL;

	static struct option long_opts[] = {
		{"pidfile",     required_argument,     0, 'p'},
		{"socket",      required_argument,     0, 'U'}, // U for Unix socket
		{"state",       required_argument,     0, 'S'},
		{"podcast_dir", required_argument,     0, 'D'},
		{"config",      required_argument,     0, 'c'},
		{"logmask",     required_argument,     0, 'l'},
		{"foreground",  no_argument,           0, 'f'},
		{"start",       no_argument,           0, 's'},
		{"kill",        no_argument,           0, 'k'},
		{"add",         required_argument,     0, 'a'},
		{"del",         no_argument,           0, 'd'},
		{"help",        no_argument,           0, 'h'},
		{0, 0, 0, 0 }
	};

	if ((cfg = cfg_from_file(DEFAULT_CONFIG_PATH)) == NULL) {
		printf("Error reading configuration file\n");
		/* We can exit cleanly here - no allocs have happened yet */
		return EXIT_FAILURE;
	}

	while(optind < argc) {
		if ((ret = getopt_long(argc, argv, "a:p:U:D:c:l:fskdh", long_opts, &b)) != -1) {
			switch (ret) {
			case 'p':
				update_cfg(cfg, PCD_STRING,
						PCD_CFG_PIDFILE,
						new_string(optarg));

				break;
			case 'U':
				update_cfg(cfg, PCD_STRING,
						PCD_CFG_SOCKET,
						new_string(optarg));

				break;
			case 'S':
				update_cfg(cfg, PCD_STRING,
						PCD_CFG_STATEFILE,
						new_string(optarg));

				break;
			case 'D':
				update_cfg(cfg, PCD_STRING,
						PCD_CFG_PODCAST_DIR,
						new_string(optarg));

				break;
			case 'c':
				user_cfg = new_string(optarg);

				break;
			case 'l':
				pcd_setlogmask(optarg);

				break;
			case 'f':
				update_cfg(cfg, PCD_BOOL,
						PCD_CFG_FOREGROUND,
						(void *) true);

				break;
			case 's':
				start = true;

				break;
			case 'k':
				kill = true;

				break;
			case 'a':
				cmd = CMD_ADD;

				url = new_string(optarg);

				break;
			case 'd':
				cmd = CMD_DELETE;

				break;
			case 'h':
				print_help(argv[0]);

				goto exit;
			}
		} else {
			break;
		}
	}

	if (start && kill) { // Ensure either start OR stop was issued
		printf("Specify either 'start' or 'stop' command\n");

		goto exit;
	}

	if (user_cfg) {
		if (update_cfg_from_file(cfg, user_cfg) != EXIT_SUCCESS) {
			err = true;
			goto exit;
		}
	}

	output_fg = cfg_get_val(cfg, PCD_CFG_FOREGROUND);

	if (start) {
		if (start_daemon(cfg) != EXIT_SUCCESS) {
			err = true;
			goto exit;
		}
	} else if (kill) {
		if (stop_daemon(cfg) != EXIT_SUCCESS) {
			err = true;
			goto exit;
		}
	} else if (cmd > 0) {
		if (!url) {
			printf("No URL given\n");

			goto exit;
		}

		if (!url_ok(str_get_buf(url))) {
			printf("Invalid URL\n");
			err = true;
			goto exit;
		}

		if (send_cmd(cfg, cmd, str_get_buf(url)) != EXIT_SUCCESS) {
			err = true;
			goto exit;
		}
	} else {
		printf("No command given.\n");

		goto exit;
	}

exit:
	cfg_destroy(cfg);

	free_string(user_cfg);

	free_string(url);

	return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
