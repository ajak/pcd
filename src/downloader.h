/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <stdbool.h>

#include <curl/curl.h>

#include "conf.h"
#include "podcast.h"

#define WAIT_TIME 3*1000

#define SIMUL_DL 2
#define CURL_TIMEOUT 5

extern struct queue *podcast_queue;
extern struct queue *ep_queue;

struct curl_easy_wrapper {
	bool running;
	bool interrupted;
	char *path;
	FILE *file;
	CURL *handle;
};

char *get_episode_ext(struct episode *e);
int queue_podcast_download(struct podcast *p);
void *download_loop(struct pcd_cfg *cfg_void);

#endif /* DOWNLOADER_H */
