/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef SOCKET_H
#define SOCKET_H

#include "command.h"

#define MAX_CONNS 3

int pcd_server_init(char *socket_path);
int pcd_client_init(char *socket_path);
int pcd_socket_send(struct cmd *command);
int pcd_socket_recv(struct cmd **command);
void pcd_client_socket_destroy(void);
void pcd_server_socket_destroy(char *socket_path);

#endif /* SOCKET_H */
