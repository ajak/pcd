/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <curl/curl.h>
#include <libxml/parser.h>

#include "conf.h"
#include "downloader.h"
#include "pcd.h"
#include "pcd_output.h"
#include "podcast.h"
#include "util.h"
#include "xml.h"

// Array of pointers to podcast structs
static struct podcast **p = {NULL};
static int pc_count = 0;

static size_t fetch_rss_cb(char *ptr, size_t size, size_t nmemb, xmlParserCtxtPtr parser)
{
	(void) size;

	xmlParseChunk(parser, ptr, (int) nmemb, 0);

	return nmemb;
}

/* In order for libxml to not output parsing errors to stderr, it needs a
 * callback function. This is it, it does nothing. */
static void err_func(void *ctx, const char *msg, ...) {
	(void) ctx;
	(void) msg;
}

static int fetch_rss(char *proxy, char *url, xmlDocPtr *doc)
{
	CURL *c = curl_easy_init();
	xmlParserCtxtPtr parser = {0};
	int ret = 0;

	parser = xmlCreatePushParserCtxt(NULL, NULL, NULL, 0, NULL);
	xmlSetGenericErrorFunc(NULL, err_func);

	if (!parser)
		return EXIT_FAILURE;

	curl_easy_setopt(c, CURLOPT_URL, url);
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, &fetch_rss_cb);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, parser);
	curl_easy_setopt(c, CURLOPT_TIMEOUT, CURL_TIMEOUT);

	// Follow redirects
	curl_easy_setopt(c, CURLOPT_FOLLOWLOCATION, 1);

	// Setup proxy options, if present
	if (proxy)
		curl_easy_setopt(c, CURLOPT_PROXY, proxy);

	ret = curl_easy_perform(c);

	switch(ret) {
	case CURLE_OK:
		break;
	case CURLE_URL_MALFORMAT:
		output(LOG_ERR, "Couldn't download %s: "
			"Malformatted URL", url);

		break;
	case CURLE_NOT_BUILT_IN:
		output(LOG_ERR, "Couldn't download %s: "
			"Feature not built into libcurl", url);

		*doc = NULL;

		break;
	case CURLE_COULDNT_CONNECT:
		output(LOG_ERR, "Couldn't download %s: "
			"Couldn't connect", url);

		*doc = NULL;

		/* If we can't connect, we exit successfully anyway
		 * because we want to add the podcast, even if we can't
		 * download now */
		goto exit;

		break;
	case CURLE_OPERATION_TIMEDOUT:
		output(LOG_ERR, "Couldn't download %s: "
			"Timed out", url);

		xmlFreeParserCtxt(parser);
		curl_easy_cleanup(c);

		*doc = NULL;

		return EXIT_FAILURE;
	default:
		output(LOG_ERR, "Couldn't download %s, error %d", url, ret);

		*doc = NULL;

		goto exit;
	}

	// Signal the end of the document
	xmlParseChunk(parser, NULL, 0, 1);

	/* TODO - Is schema validation something useful here? */
	if (parser->valid && parser->myDoc) {
		*doc = parser->myDoc;

		goto exit;
	}

	curl_easy_cleanup(c);

	return EXIT_FAILURE;

exit:
	xmlFreeParserCtxt(parser);

	curl_easy_cleanup(c);

	return EXIT_SUCCESS;
}

static int create_podcast_dir(char *podcast_dir, struct podcast *p)
{
	size_t len = strlen(podcast_dir) + strlen(p->title) + 2;
	char *path = malloc(len);

	sprintf(path, "%s/%s", podcast_dir, p->title);

	if (mkdir(path, S_IRWXU | S_IRWXG) != 0) {
		if (errno != EEXIST) {
			output(LOG_ERR, "Couldn't create directory \"%s\": %s", strerror(errno));

			free(path);

			return EXIT_FAILURE;
		}
	}

	free(path);

	return EXIT_SUCCESS;
}

static int p_try_enlarge(void)
{
	size_t num_chunks;

	if (pc_count % ARRAY_SIZE != 0) // No need to add more slots
		return EXIT_SUCCESS;

	if (!p)
		return EXIT_FAILURE;

	// This is how many multiples of ARRAY_SIZE have already been allocated
	num_chunks = (size_t) ((pc_count + 1) / ARRAY_SIZE);

	// Reallocate that many chunks + 1
	p = realloc(p, (ARRAY_SIZE * (num_chunks + 1)) * sizeof(struct podcast*));

	if (!p) {
		output(LOG_ERR, "Failed to allocate more memory to podcast array");

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int episode_free(struct episode *e)
{
	free(e->title);
	free(e->url);
	free(e->ext);

	return EXIT_SUCCESS;
}

static int podcast_free(struct podcast *pc)
{
	for (int i = 0; i < pc->e_count; i++)
		episode_free(&pc->episodes[i]);

	free(pc->episodes);
	free(pc->rss_url);
	free(pc->title);

	free(pc);

	return EXIT_SUCCESS;
}

/* TODO - this shouldn't need any more than the episode argument. The path
 * should be stored in the episode. */
char *get_episode_path(char *podcast_dir, struct episode *e)
{
	size_t len;
	char *path;

	// 13 to account for slashes, space, null terminator, and 8 character date
	// TODO - do this better so as to not use a magic number
	len = strlen(podcast_dir) +
		strlen(e->p->title) +
		strlen(e->title) +
		strlen(e->ext) + 13;

	path = malloc(len);

	// Concat the datadir with the podcast title, ISO8601 date and episode title
	sprintf(path, "%s/%s/%d%02d%02d %s%s",
		podcast_dir, e->p->title,
		e->time.tm_year + 1900, e->time.tm_mon+1, e->time.tm_mday,
		e->title, e->ext);

	return path;
}

int podcast_add(char *podcast_dir, char *proxy, char *url, bool is_new)
{
	xmlDocPtr rss_feed;

	if (!url)
		return EXIT_FAILURE;

	if (fetch_rss(proxy, url, &rss_feed) != EXIT_SUCCESS) {
		output(LOG_ERR, "Error adding %s", url);

		return EXIT_FAILURE;
	}

	if (p_try_enlarge() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	p[pc_count] = malloc(sizeof(struct podcast));
	p[pc_count]->e_count = 0;
	p[pc_count]->title = NULL;
	p[pc_count]->episodes = NULL;

	if (!p[pc_count])
		return EXIT_FAILURE;

	p[pc_count]->rss_url = strdup(url);

	/* It's possible the RSS wasn't downloaded if we couldn't connect,
	 * but we want to periodically check if it becomes possible to
	 * download in the future, so we download anyway (TODO) */
	if (rss_feed) {
		if (xml_to_podcast(podcast_dir, rss_feed, p[pc_count]) != EXIT_SUCCESS) {
			output(LOG_ERR, "Failed adding %s", url);

			xmlFreeDoc(rss_feed);
			podcast_free(p[pc_count]);

			return EXIT_FAILURE;
		}

		if (create_podcast_dir(podcast_dir, p[pc_count]) != EXIT_SUCCESS) {
			xmlFreeDoc(rss_feed);
			return EXIT_FAILURE;
		}

		queue_podcast_download(p[pc_count]);

		if (is_new)
			output(LOG_INFO, "Added %s", p[pc_count]->title);
	}

	pc_count++;

	xmlFreeDoc(rss_feed);

	return EXIT_SUCCESS;
}

static int load_podcasts(char *podcast_dir, char *proxy, char *statefile)
{
	FILE *file;
	char *line = NULL;
	size_t size = 0;

	if ((file = fopen(statefile, "r")) == NULL) {
		output(LOG_ERR, "Couldn't load podcasts from state file: %s", strerror(errno));

		return EXIT_FAILURE;
	}

	while (getline(&line, &size, file) != -1) {
		strip_newline(line);
		podcast_add(podcast_dir, proxy, line, 0);
	}

	free(line);

	fclose(file);

	return EXIT_SUCCESS;
}

static int save_podcasts(char *statefile)
{
	FILE *file;

	// Don't write an empty file if there are no podcasts
	if (pc_count == 0)
		return EXIT_SUCCESS;

	if ((file = fopen(statefile, "w")) == NULL) {
		output(LOG_ERR, "Couldn't save podcast to state file: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	for (int i = 0; i < pc_count; i++) {
		if (p[i] && p[i]->rss_url) {
			fprintf(file, "%s\n", p[i]->rss_url);
		}
	}

	fclose(file);

	return EXIT_SUCCESS;
}

int podcast_init(struct pcd_cfg *cfg)
{
	char *podcast_dir = cfg_get_val_buf(cfg, PCD_CFG_PODCAST_DIR);
	char *proxy = cfg_get_val_buf(cfg, PCD_CFG_PROXY);
	char *statefile = cfg_get_val_buf(cfg, PCD_CFG_STATEFILE);

	p = malloc(ARRAY_SIZE * sizeof(struct podcast*));

	load_podcasts(podcast_dir, proxy, statefile);

	if (!p)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

int podcast_cleanup(char *statefile)
{
	save_podcasts(statefile);

	for (int i = 0; i < pc_count; i++)
		podcast_free(p[i]);

	free(p);

	return EXIT_SUCCESS;
}
