/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>

#include "pcd.h"
#include "pcd_output.h"

/* A benefit of using these boilerplatey functions is to not check whether we
 * need to write to syslog or stdout constantly. This benefit is diminished if
 * we have to check the config to tell the function which to choose repeatedly
 * anyway. This global is a hack that lets us tell these functions one time how
 * we're running. */
bool output_fg;

/* We need a way to send exit output to pipe if we're running as a daemon or
 * output to standard output if not. This function handles that. */
int exit_output(const char *format, ...)
{
	char msg[150];
	va_list args;

	va_start(args, format);
	vsprintf(msg, format, args);
	va_end(args);

	if (output_fg)
		printf("%s\n", msg);
	else
		return write_pipe(msg);

	return EXIT_SUCCESS;
}

/* Like the previous function, this sends output to either syslog or stdout
 * based on whether program is running in foreground. */
int output(int level, const char *format, ...)
{
	char *msg;
	size_t len;
	va_list args;

	va_start(args, format);
	len = (size_t) vsnprintf(NULL, 0, format, args) + 1;
	va_end(args);

	msg = malloc(len);

	va_start(args, format);
	vsprintf(msg, format, args);
	va_end(args);

	if (!msg)
		syslog(LOG_ERR, "Couldn't allocate memory for output message");

	if (output_fg)
		printf("%s\n", msg);
	else
		syslog(level, "%s", msg);

	free(msg);

	return EXIT_SUCCESS;
}
