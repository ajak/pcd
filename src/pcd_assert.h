/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef PCD_ASSERT_H
#define PCD_ASSERT_H

#include <assert.h>

#ifdef NDEBUG
#define PCD_ASSERT(x) assert(x)
#else
#define PCD_ASSERT(x) (void)(0)
#endif /* NDEBUG */

#endif /* PCD_ASSERT_H */
