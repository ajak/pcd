/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <ctype.h> /* for isspace() */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "pcd_assert.h"
#include "pcd_string.h"

static int str_begins(char *string, char *substr)
{
	return strncmp(string, substr, strlen(substr)) == 0;
}

static size_t file_size(FILE *fp)
{
	size_t size;

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	/* size + nullptr */
	return size + 1;
}

static string *file_contents(FILE *fp, size_t size)
{
	string *f_str = malloc(sizeof(string));
	char *buf = malloc(size);
	memset(buf, 0, size);

	if (fread(buf, 1, size, fp) != size) {
		if (ferror(fp)) {
			free(buf);
			free_string(f_str);

			return NULL;
		}
	}

	f_str->len = size;
	f_str->buf = buf;

	return f_str;
}

static void cfg_val_str_set(struct pcd_cfg *cfg, string *new_val)
{
	if (cfg->val.str)
		free_string(cfg->val.str);

	cfg->val.str = new_val;
}

static void cfg_val_set(struct pcd_cfg *cfg, enum cfg_type type, enum cfg_key key, void *new_val)
{
	PCD_ASSERT(cfg.key != PCD_CFG_CONFIG);

	cfg->type = type;
	cfg->key = key;

	switch (cfg->type) {
		case PCD_BOOL:
			cfg->val.b = (bool) new_val;
			break;
		case PCD_STRING:
			cfg_val_str_set(cfg, (string *) new_val);
			break;
	}
}

/* TODO - should this return NULL in the case of an empty item? Currently to
 * check if item is empty, the caller needs to check the returned value's
 * string */
static struct pcd_cfg *cfg_val_get(struct pcd_cfg *cfg, enum cfg_key key)
{
	PCD_ASSERT(cfg.key == PCD_CFG_CONFIG);

	return &cfg->val.cfg.dict[key];
}

void *cfg_get_val(struct pcd_cfg *cfg, enum cfg_key key)
{
	PCD_ASSERT(cfg.key == PCD_CFG_CONFIG);

	return cfg_val_get(cfg, key)->val.str;
}

char *cfg_get_val_buf(struct pcd_cfg *cfg, enum cfg_key key)
{
	PCD_ASSERT(cfg.key == PCD_CFG_CONFIG);

	string *cfg_val;

	cfg_val = cfg_get_val(cfg, key);

	/* Don't try to dereference the NULL in the case that the config key
	 * doesn't exist */
	if (cfg_val)
		return cfg_val->buf;

	return NULL;
}

int update_cfg(struct pcd_cfg *cfg, enum cfg_type type, enum cfg_key key, void *new_val)
{
	struct pcd_cfg *cfg_val = cfg_val_get(cfg, key);

	cfg_val_set(cfg_val, type, key, new_val);

	return EXIT_SUCCESS;
}

static void clean_value(char *str, string **dest)
{
	char *val_buf = strdup(str);
	char *newline = strchr(val_buf, '\n');
	string *value;

	// If there's a newline, we want to new_string() to end at it, replace
	// it with a null byte
	if (newline)
		*newline = '\0';

	value = new_string(val_buf);

	free(val_buf);

	*dest = value;
}

static int cfg_process_line(struct pcd_cfg *cfg, char *str)
{
	char *variable;
	string *value;

	/* Skip leading whitespace */
	while(isspace(*str))
		str++;

	variable = str;

	if (*str == '\0')
		return EXIT_SUCCESS;

	/* Find the text after the next whitespace */
	while(!isspace(*str))
		str++;
	while(isspace(*str))
		str++;

	clean_value(str, &value);

	/* TODO - This logic needs to be refined. Checking for a space after
	 * the variable name is a poor solution to compensate for just checking
	 * if the string begins with the variable name */
	if (str_begins(variable, "user ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_USER, value);
	} else if (str_begins(variable, "socket ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_SOCKET, value);
	} else if (str_begins(variable, "pidfile ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_PIDFILE, value);
	} else if (str_begins(variable, "podcast_dir ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_PODCAST_DIR, value);
	} else if (str_begins(variable, "proxy ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_PROXY, value);
	} else if (str_begins(variable, "statefile ")) {
		update_cfg(cfg, PCD_STRING, PCD_CFG_STATEFILE, value);
	} else {
		/* TODO - output error */
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

void cfg_destroy(struct pcd_cfg *cfg)
{
	if (!cfg)
		return;

	if (cfg->key == PCD_CFG_CONFIG) {
		/* Shallow-recursively destroy sub-objects */
		for (int i = 0; i < CONFIG_DICT_LENGTH; i++)
			cfg_destroy(&cfg->val.cfg.dict[i]);

		free(cfg->val.cfg.dict);
		free(cfg);
	} else {
		switch (cfg->type) {
		case PCD_STRING:
			free_string(cfg->val.str);
			break;
		default:
			break;
		}
	}
}

static struct pcd_cfg *cfg_create(void)
{
	struct pcd_cfg *cfg = malloc(sizeof(struct pcd_cfg));

	if (!cfg)
		return NULL;

	cfg->key = PCD_CFG_CONFIG;
	cfg->val.cfg.len = CONFIG_DICT_LENGTH;
	cfg->val.cfg.dict = malloc(sizeof(struct pcd_cfg) * CONFIG_DICT_LENGTH);

	/* TODO - Instead of dumbly null-initializing the array to all 0s like
	 * this, we could instead do something like cfg_create(PCD_CFG_*) for
	 * each nested pcd_cfg */
	memset(cfg->val.cfg.dict, 0, sizeof(struct pcd_cfg) * CONFIG_DICT_LENGTH);

	// Generate some defaults
	update_cfg(cfg, PCD_STRING, PCD_CFG_SOCKET, new_string("/var/lib/pcd/pcd.socket"));
	update_cfg(cfg, PCD_STRING, PCD_CFG_PIDFILE, new_string("/var/lib/pcd/pcd.pid"));
	update_cfg(cfg, PCD_STRING, PCD_CFG_PODCAST_DIR, new_string("/var/lib/pcd"));
	update_cfg(cfg, PCD_STRING, PCD_CFG_STATEFILE, new_string("/var/lib/pcd/state"));
	update_cfg(cfg, PCD_BOOL, PCD_CFG_FOREGROUND, (void *) false);

	return cfg;
}

struct pcd_cfg *cfg_from_str(string *str)
{
	struct pcd_cfg *cfg = cfg_create();
	char *buf = str_get_buf(str);

	if (!cfg)
		return NULL;

	do {
		if (cfg_process_line(cfg, buf) == EXIT_FAILURE) {
			cfg_destroy(cfg);
			return NULL;
		}

		if (*buf == '\n')
			/* Increment the pointer so we're not pointing at the
			 * same newline when we call strchr. */
			buf++;
	} while ((buf = strchr(buf, '\n')) != NULL);

	return cfg;
}

struct pcd_cfg *cfg_from_file(char *cfg_path)
{
	FILE *cfg_file = fopen(cfg_path, "r");
	size_t f_size;
	struct pcd_cfg *cfg = NULL;
	string *f_contents;

	if (!cfg_file) {
		printf("Couldn't open %s: %s\n", cfg_path, strerror(errno));

		if (errno == ENOENT)
			return cfg_create();
		return NULL;
	}

	f_size = file_size(cfg_file);

	if ((f_contents = file_contents(cfg_file, f_size)) == NULL) {
		printf("Error reading config file\n");
	} else {
		/* Caller accepts responsibility for dealing with an error */
		cfg = cfg_from_str(f_contents);
	}

	free_string(f_contents);
	fclose(cfg_file);

	return cfg;
}

static void merge_cfgs(struct pcd_cfg *dest, struct pcd_cfg *src)
{
	PCD_ASSERT(dest->key == PCD_CFG_CONFIG);
	PCD_ASSERT(dest->src == PCD_CFG_CONFIG);

	struct pcd_cfg *src_item;
	struct pcd_cfg *dest_item;
	string *copy;

	for (int i = 0; i < CONFIG_DICT_LENGTH; i++) {
		src_item = cfg_val_get(src, (enum cfg_key) i);
		dest_item = cfg_val_get(dest, (enum cfg_key) i);

		if (src_item && src_item->val.str) {
			/* We don't want config objects to be sharing memory */
			copy = new_string(str_get_buf(src_item->val.str));

			cfg_val_set(dest_item, src_item->type, (enum cfg_key) i,
					copy);
		}
	}
}

int update_cfg_from_str(struct pcd_cfg *cfg, string *str)
{
	struct pcd_cfg *new_cfg = cfg_from_str(str);

	if (!new_cfg)
		return EXIT_FAILURE;

	merge_cfgs(cfg, new_cfg);

	cfg_destroy(new_cfg);

	return EXIT_SUCCESS;
}

int update_cfg_from_file(struct pcd_cfg *cfg, string *cfg_path)
{
	struct pcd_cfg *new_cfg = cfg_from_file(str_get_buf(cfg_path));

	if (!new_cfg)
		return EXIT_FAILURE;

	merge_cfgs(cfg, new_cfg);

	cfg_destroy(new_cfg);

	return EXIT_SUCCESS;
}
