/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef PCD_H
#define PCD_H

#include "conf.h"

#define MSG_MAXLEN 100

/* Implemented in daemon.c */
int should_run(void);
int write_pipe(char *msg);
int send_cmd(struct pcd_cfg *cfg, int cmd, char *url);
int start_daemon(struct pcd_cfg *cfg);
int stop_daemon(struct pcd_cfg *cfg);

#endif /* PCD_H */
