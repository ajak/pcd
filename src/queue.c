/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include "queue.h"

// Ternary if/elses to translate pthread_* functions' return values to the more
// portable EXIT_SUCCESS and EXIT_FAILURE
int q_get_lock(struct queue *q)
{
	return pthread_mutex_lock(&q->_mutex) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

int q_release_lock(struct queue *q)
{
	return pthread_mutex_unlock(&q->_mutex) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

/* struct queue->queue_head is intended to be a primitive linked list and is
 * implemented in queue_add and deletion is handled by handle_queue_item */
int queue_add(struct queue *q, void *data, size_t size)
{
	struct queue_item *iter_item;
	struct queue_item *new_item;

	if (!data || !size)
		return EXIT_FAILURE;

	if (q_get_lock(q) != 0)
		return EXIT_FAILURE;

	// Create new item
	new_item = malloc(sizeof(struct queue_item));

	new_item->next = NULL;

	// Make sure we're not putting stack variables from other threads into
	// this queue's thread
	// TODO - It isn't a queue's responsibility to handle this. Refactor it away.
	new_item->data = malloc(size);
	memcpy(new_item->data, data, size);

	if (q->queue_head) { // Do this if we're not creating the head item
		iter_item = q->queue_head;

		// Seek forward in linked list to find item with a null next item
		while (iter_item->next)
			iter_item = iter_item->next;

		// Then set that item's next pointer to the new item
		iter_item->next = new_item;
	} else { // Do this if we're creating the head item
		q->queue_head = new_item;
	}

	if (q_release_lock(q) != 0)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

void *q_get_item(struct queue *q)
{
	struct queue_item *head;
	void *ret = NULL;

	q_get_lock(q);

	if (q->queue_head) {
		head = q->queue_head;
		ret = q->queue_head->data;

		// Detatch the old head and free
		q->queue_head = q->queue_head->next;
		free(head);
	}

	q_release_lock(q);

	return ret;
}

struct queue *queue_init(void (*free)(void *data))
{
	struct queue *q;

	q = malloc(sizeof(struct queue));

	// Exit here if malloc failed to avoid null pointer SIGSEGV's when
	// assigning to q fields
	if (!q)
		return NULL;

	q->queue_head = NULL;

	if (free)
		q->free = free;
	else
		q->free = NULL;

	// Setup pthread vars
	if (pthread_cond_init(&q->cond, NULL) != 0)
		return NULL;

	if (pthread_mutex_init(&q->_mutex, NULL) != 0)
		return NULL;

	return q;
}

void queue_cleanup(struct queue *q)
{
	struct queue_item *item;
	struct queue_item *next;

	if (!q)
		return;

	next = q->queue_head;

	while (next) {
		item = next;

		next = item->next;

		if (q->free)
			q->free(item->data);

		free(item);
	}

	pthread_cond_destroy(&q->cond);
	pthread_mutex_destroy(&q->_mutex);

	free(q);
}
