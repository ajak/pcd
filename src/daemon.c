/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <pthread.h>
#include <pwd.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#include "conf.h"
#include "downloader.h"
#include "pcd.h"
#include "pcd_output.h"
#include "pcd_socket.h"
#include "queue.h"
#include "util.h"

volatile int run = 1;
static int pipe_fd[2];

int should_run(void)
{
	return run;
}

static void term(int unused)
{
	(void) unused;

	output(LOG_INFO, "Exiting.");
	run = 0;
}

static int dir_exists(char *path)
{
	struct stat s;

	if (stat(path, &s) == 0)
		return S_ISDIR(s.st_mode);

	return 0;
}

static int write_pidfile(char *pidfile)
{
	int fd;
	char pid_str[12] = {0};

	if (file_exists(pidfile)) {
		exit_output("Daemon is already running. However, if you are "
			"certain is is not, delete '%s'.", pidfile);
		return EXIT_FAILURE;
	}

	if ((fd = open(pidfile,
			O_CREAT | O_RDWR,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1) { /* Problem opening the file */
		exit_output("Couldn't open pidfile at %s: %s", pidfile, strerror(errno));

		return EXIT_FAILURE;
	}

	/* Convert getpid()'s long type to a char* stored in pid_str */
	snprintf(pid_str, sizeof(pid_str), "%d", getpid());

	/* Write to file */
	if (write(fd, pid_str, sizeof(pid_str)) == -1) {
		output(LOG_ERR, "Couldn't write pidfile.");
		return EXIT_FAILURE;
	}

	close(fd);

	return EXIT_SUCCESS;
}

static int create_datadir(struct pcd_cfg *cfg)
{
	char *podcast_dir = cfg_get_val_buf(cfg, PCD_CFG_PODCAST_DIR);

	if (dir_exists(podcast_dir))
		return EXIT_SUCCESS;

	// Try to create data directory with mode 770
	if (mkdir(podcast_dir, S_IRWXU | S_IRWXG) != 0) {
		output(LOG_ERR, "Couldn't create data directory: %s",
				strerror(errno));

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int perms_ok(struct pcd_cfg *cfg)
{
	struct stat buf = {0};
	char *pidfile_path = cfg_get_val_buf(cfg, PCD_CFG_PIDFILE);
	char *socket_path = cfg_get_val_buf(cfg, PCD_CFG_SOCKET);
	char *podcast_dir = cfg_get_val_buf(cfg, PCD_CFG_PODCAST_DIR);
	char dir[100];

	// Stat the pidfile path
	if (stat(pidfile_path, &buf) == -1) {
		// If path doesn't exist, we can continue even if this fails
		if (errno != ENOENT)
			return EXIT_FAILURE;
	}

	if (S_ISDIR(buf.st_mode)) {
		exit_output("Configured pidfile path is a directory and cannot "
			"be used as a pidfile.");

		return EXIT_FAILURE;
	}

	// Get directory of pidfile
	strcpy(dir, pidfile_path);
	dirname(dir);

	// Check if writable by this process
	if (access(dir, W_OK) == -1) {
		// Can't write to pidfile
		exit_output("Permission denied on pidfile at '%s'", pidfile_path);

		return EXIT_FAILURE;
	}

	// Get socketfile directory
	strcpy(dir, socket_path);
	dirname(dir);

	if (access(dir, W_OK) == -1) {
		// Can't write to socketfile
		exit_output("Permission denied on socketfile at '%s'",
			socket_path);

		return EXIT_FAILURE;
	}

	// Get directory of datadir
	strcpy(dir, podcast_dir);
	dirname(dir);

	if (access(dir, W_OK) == -1) {
		exit_output("Permission denied on datadir at '%s'",
			podcast_dir);

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int drop_priv(char *user)
{
	struct passwd *pass;
	struct group *default_grp;

	// Get UID of the user to drop to
	pass = getpwnam(user);

	if (pass) {
		// If already running as desired user, exit
		if (getuid() == pass->pw_uid)
			return EXIT_SUCCESS;

		// If we're configured to _raise_ privileges to root, that isn't
		// possible, exit
		if (pass->pw_uid == 0) {
			exit_output("Can't raise to root privileges");

			return EXIT_FAILURE;
		}

 		if (setgid(pass->pw_uid) == -1) {
			// User's UID may not correspond to a group, try setting
			// to 'users' group
			default_grp = getgrnam("users");

			if (setgid(default_grp->gr_gid) == -1) {
				exit_output("Couldn't set group to '%s'",
					default_grp->gr_name);

				return EXIT_FAILURE;
			}
		}

		if (setuid(pass->pw_uid) == -1) {
			exit_output("Couldn't set owner to '%s'", pass->pw_name);

			return EXIT_FAILURE;
		}

		// Change directory to user's home directory
		if (chdir(pass->pw_dir) == -1) {
			exit_output("Couldn't change directory to '%s'", pass->pw_dir);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

static int close_filedescs(void)
{
#ifndef DEBUG
	struct dirent *dp;
	DIR *dir;
	char buf[64]; // 64?
	int fd;

	snprintf(buf, 64, "/proc/self/fd/");
	dir = opendir(buf);

	// Loop through dirs in /proc/self/fd/ and close fd's that arent stdout,
	// stdin, or stderr
	while ((dp = readdir(dir)) != NULL) {
		fd = (int) strtol(dp->d_name, NULL, 10);

		if (fd != 0 && fd != 1 && fd != 2) {
			if(close(fd) != 0) { // close failed
				printf("%s\n", strerror(errno));

				closedir(dir);

				return EXIT_FAILURE;
			}
		}
	}

	closedir(dir);
#endif /* DEBUG */

	return EXIT_SUCCESS;
}

static int prepare_env(void)
{
	sigset_t sigset;

	/* Close all file descriptors except stdin, stdout, stderr */
	if (close_filedescs() != EXIT_SUCCESS) {
		printf("Error closing file descriptors\n");
		return EXIT_FAILURE;
	}

	/* Reset all signal handlers to defaults */
	for (int sig = SIGRTMIN; sig <= SIGRTMAX; sig++) {
		if (signal(sig, SIG_DFL) == SIG_ERR) {
			printf("Invalid signum passed to signal()\n");

			return EXIT_FAILURE;
		}
	}

	sigfillset(&sigset);

	if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) == -1)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

static int redirect_fds(void)
{
	/* Close standard FD's */
	close(0);
	close(1);
	close(2);

	/* Reopen them with error checking */
	if ((open("/dev/null", O_RDONLY) != 0)
		|| (open("/dev/null", O_RDWR) != 1)
		|| (open("/dev/null", O_RDWR) != 2)) {

		exit_output("Error redirecting standard file descriptors");

		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int write_pipe(char *msg)
{
	string *msg_str = new_string(msg);

	if (write(pipe_fd[1], msg_str, sizeof(msg_str)) == -1) {
		output(LOG_ERR, "Can't write to pipe: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int read_pipe(string *msg)
{
	if (read(pipe_fd[0], msg, sizeof(msg)) == -1) {
		output(LOG_ERR, "Can't read from pipe: %s", strerror(errno));
	} else {
		return EXIT_SUCCESS;
	}

	return EXIT_SUCCESS;
}

static int handle_cmd(struct cmd command)
{
	if (command.cmd < 0) {
		output(LOG_ERR, "Bogus command sent to daemon");
		return EXIT_FAILURE;
	}

	if (command.cmd == CMD_HALT) {
		term(0);
	} else if (command.cmd == CMD_ADD) {
		if (*command.url == '\0')
			return EXIT_FAILURE;
	}

	return queue_add(podcast_queue, &command, sizeof(command));
}

int send_cmd(struct pcd_cfg *cfg, int cmd, char *url)
{
	struct cmd command = {0};
	char *socket_path = cfg_get_val_buf(cfg, PCD_CFG_SOCKET);

	// Set up command struct
	command.cmd = cmd;

	if (url)
		strcpy(command.url, url);

	// Create client socket
	if (pcd_client_init(socket_path) == EXIT_FAILURE)
		return EXIT_FAILURE;

	if (pcd_socket_send(&command) == EXIT_FAILURE)
		return EXIT_FAILURE;

	pcd_client_socket_destroy();

	return EXIT_SUCCESS;
}

static void *message_loop(void *unused)
{
	struct cmd *command = malloc(sizeof(*command));
	(void) unused;

	output(LOG_INFO, "Message loop starting");

	while (should_run()) {
		if (pcd_socket_recv(&command) == EXIT_FAILURE)
			break;

		handle_cmd(*command);
	}

	output(LOG_INFO, "Message loop terminated");

	free(command);

	return NULL;
}

static int start_threads(struct pcd_cfg *cfg)
{
	char *pidfile_path = cfg_get_val_buf(cfg, PCD_CFG_PIDFILE);
	char *socket_path = cfg_get_val_buf(cfg, PCD_CFG_SOCKET);
	pthread_t ipc_thread;
	pthread_t dl_thread;

	// Handle SIGTERM
	signal(SIGTERM, &term);

	if (pcd_server_init(socket_path) != EXIT_SUCCESS) {
		exit_output("Error opening socket at %s", socket_path);

		return EXIT_FAILURE;
	}

	// Start loop that handles commands issued from user
	if (pthread_create(&ipc_thread, NULL, (void *(*)(void *)) &message_loop, cfg) != 0)
		return EXIT_FAILURE;

	// Start download thread
	if (pthread_create(&dl_thread, NULL, (void *(*)(void *)) &download_loop, cfg) != 0)
		return EXIT_FAILURE;

	// Tell parent process that it's safe to exit
	exit_output("Daemon started successfully");

	pthread_join(dl_thread, NULL);
	pthread_join(ipc_thread, NULL);

	// Remove the pidfile
	unlink(pidfile_path);

	pcd_server_socket_destroy(socket_path);

	return EXIT_SUCCESS;
}

static int launch(struct pcd_cfg *cfg)
{
	bool fg = cfg_get_val(cfg, PCD_CFG_FOREGROUND);

	// Only run this if we're running as a daemon
	if (!fg && redirect_fds() != EXIT_SUCCESS) {
		cfg_destroy(cfg);
		exit(EXIT_FAILURE);
	}

	// Set umask to 0
	umask(0);

	// Check to see if necessary files are writable by run_as_user
	if (perms_ok(cfg) != EXIT_SUCCESS) {
		cfg_destroy(cfg);
		exit(EXIT_FAILURE);
	}

	if (create_datadir(cfg) != EXIT_SUCCESS) {
		cfg_destroy(cfg);
		exit(EXIT_FAILURE);
	}

	// Write pidfile
	if (write_pidfile(cfg_get_val_buf(cfg, PCD_CFG_PIDFILE)) != EXIT_SUCCESS) {
		cfg_destroy(cfg);
		exit(EXIT_FAILURE);
	}

	output(LOG_INFO, "PCD Starting");

	return start_threads(cfg);
}

/* TODO - implement this for systemd */
int start_daemon(struct pcd_cfg *cfg)
{
	string *msg = NULL;
	char *user = cfg_get_val_buf(cfg, PCD_CFG_USER);
	bool fg = cfg_get_val(cfg, PCD_CFG_FOREGROUND);

	if (prepare_env() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	/* Only try to change process ownership if there is a user configured */
	if (user) {
		if (drop_priv(user) != EXIT_SUCCESS) {
			cfg_destroy(cfg);
			exit(EXIT_FAILURE);
		}
	}

	if (fg)
		return launch(cfg);

	if (pipe(pipe_fd) == -1) {
		output(LOG_ERR, "Failed creating pipe\n");

		return EXIT_FAILURE;
	}

	switch(fork()) {
	case 0:
		setsid();

		switch(fork()) {
		case 0:
			return launch(cfg);
			break;
		case -1:
			return EXIT_FAILURE;
			break;
		default:
			return EXIT_SUCCESS;
			break;
		}

		break;
	case -1:
		return EXIT_FAILURE;
		break;
	default:
		if (read_pipe(msg) == EXIT_SUCCESS) {
			close(pipe_fd[0]);
			close(pipe_fd[1]);

			printf("%s\n", str_get_buf(msg));

			free_string(msg);

			return EXIT_SUCCESS;
		} else {
			return EXIT_FAILURE;
		}

		break;
	}

	return EXIT_SUCCESS;
}

int stop_daemon(struct pcd_cfg *cfg)
{
	char *pidfile_path = cfg_get_val_buf(cfg, PCD_CFG_PIDFILE);

	if (access(pidfile_path, F_OK == -1)) {
		printf("Daemon not running.\n");

		return EXIT_FAILURE;
	}

	if (send_cmd(cfg, CMD_HALT, NULL) == EXIT_SUCCESS) {
		// TODO - This could loop forever if the daemon somehow fails to
		// remove the pidfile
		while (access(pidfile_path, F_OK) == 0) {
			sched_yield();
		}

		printf("Daemon exited successfully\n");

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}
