/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <syslog.h>
#include <unistd.h>

#include "command.h"
#include "conf.h"
#include "pcd_assert.h"
#include "pcd_output.h"
#include "pcd_socket.h"

static int pcd_socket_fd = -1;
static struct sockaddr_un pcd_socket_addr;

int pcd_server_init(char *socket_path)
{
	PCD_ASSERT(pcd_socket_fd == -1);

	socklen_t len;

	// Create socket
	if ((pcd_socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		output(LOG_ERR, "Failed to create Unix socket: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	// Initialize Unix socket address struct
	pcd_socket_addr.sun_family = AF_UNIX;
	strcpy(pcd_socket_addr.sun_path, socket_path);

	len = (socklen_t) (strlen(pcd_socket_addr.sun_path) + sizeof(pcd_socket_addr.sun_family));

	if (bind(pcd_socket_fd, (struct sockaddr *) &pcd_socket_addr, len) == -1) {
		output(LOG_ERR, "Failed to bind to Unix socket: %s", strerror(errno));

		close(pcd_socket_fd);

		return EXIT_FAILURE;
	}

	if (listen(pcd_socket_fd, MAX_CONNS) == -1) {
		output(LOG_ERR, "Failed to listen to socket: %s", strerror(errno));

		return EXIT_FAILURE;
	}


	return EXIT_SUCCESS;
}

int pcd_client_init(char *socket_path)
{
	PCD_ASSERT(pcd_socket_fd == -1);
	socklen_t len;

	if ((pcd_socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		output(LOG_ERR, "Failed to connect to pcd: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	// Init addr struct
	pcd_socket_addr.sun_family = AF_UNIX;
	strcpy(pcd_socket_addr.sun_path, socket_path);

	len = (socklen_t) (strlen(pcd_socket_addr.sun_path) + \
			sizeof(pcd_socket_addr.sun_family));

	if (connect(pcd_socket_fd, (struct sockaddr *) &pcd_socket_addr, len) == -1) {
		output(LOG_ERR, "Failed to connect to pcd: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int pcd_socket_send(struct cmd *command)
{
	PCD_ASSERT(pcd_socket_fd != -1);

	if (send(pcd_socket_fd, command, sizeof(*command), 0) == -1) {
		output(LOG_ERR, "Failed to communicate with pcd: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int pcd_socket_recv(struct cmd **command)
{
	struct sockaddr_un *recv_sock;
	socklen_t size = sizeof(*recv_sock);
	int recv_fd;

	if ((recv_fd = accept(pcd_socket_fd, (struct sockaddr *) &recv_sock, &size)) == -1) {
		output(LOG_ERR, "Error accepting connection: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	if (recv(recv_fd, *command, sizeof(**command), 0) == -1)
		output(LOG_ERR, "Error receiving data from socket: %s", strerror(errno));

	close(recv_fd);

	return EXIT_SUCCESS;
}

void pcd_client_socket_destroy(void)
{
	/* Does an error here need to be checked? */
	close(pcd_socket_fd);
}

void pcd_server_socket_destroy(char *socket_path)
{
	pcd_client_socket_destroy();
	unlink(socket_path);
}
