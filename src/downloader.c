/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <curl/curl.h>

#include "conf.h"
#include "command.h"
#include "downloader.h"
#include "pcd_output.h"
#include "queue.h"
#include "util.h"
#include "xml.h"

/* Podcasts that need to be processed */
struct queue *podcast_queue;

/* Podcasts that need to be downloaded */
struct queue *ep_queue;

static CURLM *curlm;

/* TODO - make SIMUL_DL configurable */
static struct curl_easy_wrapper *easys[SIMUL_DL];

/* We don't have an easy way to abort an in-progress transfer, but it
 * will abort it if this function returns something other than what it
 * expects, which is size * nmemb.
 */
static int write_func(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	fwrite(ptr, size, nmemb, (FILE*) ((struct curl_easy_wrapper *) (userdata))->file);

	if (!should_run()) {
		// Signal that this handle is unfinished and should be removed
		((struct curl_easy_wrapper *) userdata)->interrupted = true;
		return -1;
	}

	return (int) (size * nmemb);
}

static struct curl_easy_wrapper *get_avail_wrapper(void)
{
	for (int i = 0; i < SIMUL_DL; i++)
		if (!easys[i]->running)
			return easys[i];

	return NULL;
}

static int setup_handle(CURL *c, char *url, struct curl_easy_wrapper *easy, char *proxy)
{
	curl_easy_reset(c);
	curl_easy_setopt(c, CURLOPT_URL, url);
	curl_easy_setopt(c, CURLOPT_PRIVATE, easy);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, easy);
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, write_func);
	curl_easy_setopt(c, CURLOPT_FOLLOWLOCATION, 1);

	if (proxy)
		curl_easy_setopt(c, CURLOPT_PROXY, proxy);

	return EXIT_SUCCESS;
}

/* This information should be exposed in podcast.c */
char *get_episode_ext(struct episode *e)
{
	CURLU *cu;
	char *dot;
	char *path;

	cu = curl_url();

	if (!cu)
		return NULL;

	curl_url_set(cu, CURLUPART_URL, e->url, 0);
	curl_url_get(cu, CURLUPART_PATH, &path, 0);

	if (!path)
		return NULL;

	// https://stackoverflow.com/a/5309508
	// Need to free path, so we'll go ahead and dupe it here
	dot = strdup(strrchr(path, '.'));

	curl_free(path);
	curl_url_cleanup(cu);

	return dot;
}

static int handle_dl_item(struct episode *e, struct pcd_cfg *cfg)
{
	struct curl_easy_wrapper *easy;
	FILE *file;
	char *path;

	if (!e || !e->p || !e->title || !e->url)
		return EXIT_FAILURE;

	path = get_episode_path(cfg_get_val_buf(cfg, PCD_CFG_PODCAST_DIR), e);

	if (file_exists(path)) {
		free(path);
		return EXIT_SUCCESS;
	}

	file = fopen(path, "w");

	if (!file) {
		free(path);
		return EXIT_FAILURE;
	}

	easy = get_avail_wrapper();

	if (!easy) {
		fclose(file);
		free(path);
		return EXIT_FAILURE;
	}

	// Set up an easy handle to add to curlm
	setup_handle(easy->handle, e->url, easy, cfg_get_val_buf(cfg, PCD_CFG_PROXY));

	easy->file = file;
	easy->path = path;

	curl_multi_add_handle(curlm, easy->handle);

	easy->running = true;

	return EXIT_SUCCESS;
}

static int curl_clean_dl(void)
{
	CURLMsg *msg;
	CURL *handle;
	struct curl_easy_wrapper *easy = NULL;
	int msgs_left = 0;

	while ((msg = curl_multi_info_read(curlm, &msgs_left))) {
		if (msg->msg == CURLMSG_DONE) {
			handle = msg->easy_handle;

			curl_easy_getinfo(handle, CURLINFO_PRIVATE, &easy);

			if (easy->interrupted)
				unlink(easy->path);
			else
				output(LOG_INFO, "Downloaded %s", easy->path);

			free(easy->path);
			fclose(easy->file);

			easy->path = NULL;
			easy->file = NULL;

			curl_multi_remove_handle(curlm, handle);

			easy->running = false;
		}
	}

	return EXIT_SUCCESS;
}

static int handle_new_dl(struct cmd *c, struct pcd_cfg *cfg)
{
	char *podcast_dir = cfg_get_val_buf(cfg, PCD_CFG_PODCAST_DIR);
	char *proxy = cfg_get_val_buf(cfg, PCD_CFG_PROXY);

	if (c->cmd == CMD_ADD)
		return podcast_add(podcast_dir, proxy, c->url, 1);

	return EXIT_FAILURE;
}

static int try_handle_new_podcast(struct pcd_cfg *cfg)
{
	struct cmd *c = NULL;

	/* Returns NULL if there's nothing on-queue */
	c = q_get_item(podcast_queue);

	if (c) {
		handle_new_dl(c, cfg);

		// TODO - see todo in queue_add
		free(c);

		c = NULL;
	}

	return EXIT_SUCCESS;
}

int queue_podcast_download(struct podcast *p)
{
	// To prevent the sizeof calculation for the following for loop
	size_t size = sizeof(struct episode);

	for (int i = 0; i < p->e_count; i++) {
		if (queue_add(ep_queue, &p->episodes[i], size) != EXIT_SUCCESS)
			return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int download_setup(struct pcd_cfg *cfg)
{
	if ((podcast_queue = queue_init(&free)) == NULL)
		return EXIT_FAILURE;

	if ((ep_queue = queue_init(&free)) == NULL)
		return EXIT_FAILURE;

	if (curl_global_init(CURL_GLOBAL_DEFAULT) != 0)
		return EXIT_FAILURE;

	curlm = curl_multi_init();

	xmlInitParser();

	for (int i = 0; i < SIMUL_DL; i++) {
		easys[i] = calloc(1, sizeof(struct curl_easy_wrapper));

		if (!easys[i])
			return EXIT_FAILURE;

		easys[i]->running = false;
		easys[i]->interrupted = false;
		easys[i]->handle = curl_easy_init();

		if (easys[i]->handle == NULL)
			return EXIT_FAILURE;
	}

	if (podcast_init(cfg) != EXIT_SUCCESS)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

static void download_destroy(struct pcd_cfg *cfg)
{
	if (podcast_cleanup(cfg_get_val_buf(cfg, PCD_CFG_STATEFILE)) != EXIT_SUCCESS)
		output(LOG_ERR, "Error cleaning up podcast data");

	for (int i = 0; i < SIMUL_DL; i++) {
		curl_easy_cleanup(easys[i]->handle);

		if (easys[i]->file)
			fclose(easys[i]->file);

		free(easys[i]->path);
		free(easys[i]);
	}

	xmlCleanupParser();

	curl_multi_cleanup(curlm);
	curl_global_cleanup();

	queue_cleanup(ep_queue);
	queue_cleanup(podcast_queue);
}

void *download_loop(struct pcd_cfg *cfg)
{
	struct episode *e = NULL;
	int in_progress = 0;
	int fds = 0;

	download_setup(cfg);

	output(LOG_INFO, "Download loop starting");

	while (should_run()) {
		if ((in_progress < 2) && should_run()) {
			e = q_get_item(ep_queue);

			if (e)
				handle_dl_item(e, cfg);

			// TODO - see todo in queue_add
			free(e);

			e = NULL;
		}

		try_handle_new_podcast(cfg);

		curl_multi_perform(curlm, &in_progress);

		curl_clean_dl();

		curl_multi_wait(curlm, NULL, 0, WAIT_TIME, &fds);
	}

	output(LOG_INFO, "Download loop terminated");

	download_destroy(cfg);

	return NULL;
}
